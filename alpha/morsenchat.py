import sys
import time
import numpy as np
import pyaudio
from colorama import just_fix_windows_console, Fore, Style
import morsecodetables as mct

just_fix_windows_console() # enables cross-platform colored console output

# main menu options
MAIN_MENU_OPTIONS = {
    'help': 'Shows all options of the application',
    'translate': 'Translates latin character strings to morse code (default) and vice versa',
    'listen': 'Converts latin character strings and morse code to morse audio feedback',
    'exit': 'Exit the Program'
}

# translate options
MODE_TRANSLATE_OPTIONS = {
    'ltm': 'Latin character strings to morse code',
    'mtl': 'Morse code to latin characters',
    'back': 'Back to main menu'
}

# listen options
MODE_LISTEN_OPTIONS = {
    'ltmta': 'Latin character stringgit to morse code to audio',
    'back': 'Back to main menu'
}

def showAppInformations():
    print("Developer of morse.n.chat are Anna Wirbel, David Melamed, Pete Ambos.")
    print("This application translate latin character strings to morse code and vice versa, \n and enables creating morse audio feedback from latin strings.")
    print(" ")

def showOptions():
    print("Options:") # functions of alpha 0.0.1
    print("   translate - Latin character strings to morse code (default) and vice versa") # functions of alpha 0.0.1
    print("   listen - Morse code to audio feedback") # functions of alpha 0.0.2
    print("   exit - Exit the program")   

def generateSound(symbol):
    p = pyaudio.PyAudio()
    volume = 0.5  # range [0.0, 1.0]
    fs = 50000  # sampling rate, Hz, must be integer
    f = 440.0  # sine frequency, Hz, may be float
    if symbol == ".":
        duration = 0.1
    elif symbol == "-":
        duration = 0.3
    
     # generate samples, note conversion to float32 array
    samples = (np.sin(2 * np.pi * np.arange(fs * duration) * f / fs)).astype(np.float32)

    # per @yahweh comment explicitly convert to bytes sequence
    output_bytes = (volume * samples).tobytes()

    # for paFloat32 sample values must be in range [-1.0, 1.0]
    stream = p.open(format=pyaudio.paFloat32,
                    channels=1,
                    rate=fs,
                    output=True)

    # play. May repeat with different volume values (if done interactively)
    start_time = time.time()
    stream.write(output_bytes)
    stream.stop_stream()
    stream.close()
    p.terminate()

def morseToAudio(morse):
    symbols = morse.replace("   ", " SPACE ").split(" ")
    for x in symbols:
        if x == "SPACE":
            time.sleep(0.9)
        else:
            for y in x:
                generateSound(y)
                time.sleep(.05) 
            
def latinToMorse(input):
    try:
        morse = " ".join(mct.encode_table[x] for x in input.upper())
    except KeyError:
        print(Fore.RED + "Error: One or more characters do not have morse code equivalents. Valid characters with morse equivalents are \"A-Z\", \"0-9\", \" \", \".\", \",\", and \"?\"." + Style.RESET_ALL)
        return ""
    return morse.replace(" SPACE ", "   ")


def morseToLatin(input):
    symbols = input.replace("   ", " SPACE ").split(" ")
    return "".join(mct.decode_table[x] for x in symbols)

        
def latinToMorseToAudio(input): #unused
    
    print("\"" + input + "\" but translated to Audio")

# stack to track modes
mode_stack = []
    
# display menu in termianl    
def display_menu(options):
    print("Menu:")
    for option, info in options.items():
        print(f"   {option}: {info}")
    print()
    
    
def main_menu():
    showAppInformations()
    while True:
        display_menu(MAIN_MENU_OPTIONS)
        menuChoice = input("Enter one option: ").lower()
        print()
        if menuChoice == 'help':
            showOptions()
        elif menuChoice == 'translate':
            mode_stack.append(main_menu)
            mode_translate_menu()
        elif menuChoice == 'listen':
            mode_stack.append(main_menu)
            mode_listen_menu()
        elif menuChoice == 'exit':
            print("Exiting program.")
            sys.exit()
        else: 
            print("Invalid choice. Please try again.")
            
def mode_translate_menu():
    while True:
        display_menu(MODE_TRANSLATE_OPTIONS)
        menuChoice = input("Enter your choice: ")
        userInput = None
        
        if menuChoice == 'ltm':
            userInput = input("Enter an string of latin characters to translate to morse code: ")
            retVal = latinToMorse(userInput)
            print(retVal)
            userInput = None # clear value
        elif menuChoice == 'mtl':
            userInput = input("Enter morse code to translate to latin characters: ")
            retVal = morseToLatin(userInput)
            print(retVal)
            userInput = None 
        elif menuChoice == 'back':
            if mode_stack:
                mode = mode_stack.pop()
                mode()
            else:
                print("No previous mode. Returning to Main Menu.")
                main_menu()
        else:
            print("Invalid choice. Please try again.")
            
def mode_listen_menu():
    while True:
        display_menu(MODE_LISTEN_OPTIONS)
        userChoice = input("Enter your choice: ")
        userInput = None
        
        if userChoice == 'ltmta':
            userInput = input("Enter an string of latin characters to translate to Morse code and get audio feedback: ")
            morse = latinToMorse(userInput)
            print(morse)
            morseToAudio(morse)
        elif userChoice == 'back':
            if mode_stack:
                mode = mode_stack.pop()
                mode()
            else:
                print("No previous mode. Returning to Main Menu.")
                main_menu()
        else:
            print("Invalid choice. Please try again.")

def main():
    main_menu()


if __name__ == "__main__":
    main()